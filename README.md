# junior2 project image



## Getting started

Clone this repository

```
git clone https://gitlab.com/gabrieldiaz94/junior2-project-image.git
```

## Build, Run and Exec the container

- Run the container inside the deployment folder, for easy use of this container run the bash file

```
source run_image.sh
```

## Close the container
```
source close_container.sh
```
## Build and run the package

- Inside the container in the ras_ws folder run: 
```
source init.sh
```

## Launch one of the packages

- This package contain the practice of tf, urdf, etc of Junior2 class.

```
roslaunch urdf_tutorial_ras display.launch
```