FROM osrf/ros:foxy-desktop


# Change the default shell to Bash
SHELL [ "/bin/bash" , "-c" ]

# Install packages
RUN apt-get install curl -y \
&& curl -sSL https://raw.githubusercontent.com/ros/rosdistro/master/ros.key -o /usr/share/keyrings/ros-archive-keyring.gpg \
&& echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/ros-archive-keyring.gpg] http://packages.ros.org/ros2/ubuntu $(. /etc/os-release && echo $UBUNTU_CODENAME) main" | tee /etc/apt/sources.list.d/ros2.list > /dev/null \
&& apt-get update \
&& apt-get upgrade -y \
&& apt-get install -y \
nano \
# Clear apt-cache to reduce image size
&& rm -rf /var/lib/apt/lists/*

ENV RAS_WS=/app/ras_ws
COPY app app/
WORKDIR $RAS_WS

RUN echo "source /opt/ros/foxy/setup.bash" >> /root/.bashrc 
